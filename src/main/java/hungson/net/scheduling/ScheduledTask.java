package hungson.net.scheduling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Component
public class ScheduledTask {

    private static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Scheduled(fixedRate = 2000)
    public void demoSchedule() {
        log.info("ThreadName - {} :: Execution Time - {}", Thread.currentThread().getName(), dateFormat.format(new Date()));
    }

//    @Scheduled(fixedRate = 2000)
//    public void scheduleTaskWithFixedRate() {
//        log.info("Fixed Rate Task :: Execution Time - {}", dateFormat.format(new Date()));
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException ex) {
//            log.error("Ran into an error {}", ex);
//        }
//    }

//    @Scheduled(fixedDelay = 2000)
//    public void scheduleTaskWithFixedDelay() {
//        log.info("Fixed Delay Task :: Execution Time - {}", dateFormat.format(new Date()));
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch (InterruptedException ex) {
//            log.error("Ran into an error {}", ex);
//        }
//    }

//    @Scheduled(fixedRate = 2000, initialDelay = 5000)
//    public void scheduleTaskWithFixedDelay() {
//        log.info("Fixed Rate with initialDelay :: Execution Time - {}", dateFormat.format(new Date()));
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch (InterruptedException ex) {
//            log.error("Ran into an error {}", ex);
//        }
//    }

//    @Scheduled(cron = "0/30 * * * * ?")
//    public void scheduleTaskWithCronExpression() {
//        log.info("Cron Task :: Execution Time - {}", dateFormat.format(new Date()));
//    }

}
