package hungson.net.mysql;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;

public interface StudentJPARepository extends JpaRepository<Student, Integer> {

    @Query(value = "SELECT s.* FROM student s WHERE s.student_name=:student_name", nativeQuery = true)
    public List<Student> findByName(@Param("student_name") String studentName);

}
