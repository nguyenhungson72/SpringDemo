package hungson.net.mysql;

import javax.persistence.*;

@Entity
@Table(name = "student")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "get_student_by_name",
                resultClasses = Student.class,
                procedureName = "getStudentByName",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "studentName", type = String.class)
                }
        )
})
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int student_id;

    private String student_name;

    private String class_name;

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }
}
