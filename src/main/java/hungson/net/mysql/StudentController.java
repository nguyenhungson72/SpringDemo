package hungson.net.mysql;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class StudentController {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private StudentJPARepository studentJPARepository;

    @RequestMapping(method = RequestMethod.POST, value = "/student")
    public String actionStudent(@RequestBody Student params) {
        studentRepository.save(params);
        return "Added successful";
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getall")
    public @ResponseBody Iterable<Student> getAllUsers() {
        // This returns a JSON or XML with the users
        return studentRepository.findAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getbyid")
    public @ResponseBody Optional<Student> getById(@RequestParam int id) {
        // This returns a JSON or XML with the users
        return studentRepository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getbyname")
    public @ResponseBody List<Student> getByName(@RequestParam String studentName) {
        // This returns a JSON or XML with the users
        return studentJPARepository.findByName(studentName);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/getbynamesp")
    public @ResponseBody List<Student> getByNameSP(@RequestParam String studentName) {
        // This returns a JSON or XML with the users
        Iterable<Student> students = studentRepository.findByName(studentName);
        List<Student> listStudent = new ArrayList<>();
        students.forEach(listStudent::add);
        return listStudent;
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete")
    public void deleteStudent(@RequestParam int id) {
        studentRepository.deleteById(id);
    }

}
