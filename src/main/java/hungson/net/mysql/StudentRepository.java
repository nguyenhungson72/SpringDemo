package hungson.net.mysql;

import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<Student, Integer>, StudentRepositoryCustom { }

