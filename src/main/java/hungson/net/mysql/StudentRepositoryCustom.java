package hungson.net.mysql;

import java.util.List;

public interface StudentRepositoryCustom {

    List<Student> findByName(String studentName);

}
