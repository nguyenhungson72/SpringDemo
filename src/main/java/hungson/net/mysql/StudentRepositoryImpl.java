package hungson.net.mysql;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

public class StudentRepositoryImpl implements StudentRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Student> findByName(String studentName) {
        StoredProcedureQuery findByNameSP = em.createNamedStoredProcedureQuery("get_student_by_name");
        findByNameSP.setParameter("studentName", studentName);
        return findByNameSP.getResultList();
    }
}
