package hungson.net.restful;

import org.springframework.web.bind.annotation.*;

@RestController
public class HelloController {

    @RequestMapping(method = RequestMethod.GET, value = "/hello")
    public HelloEntity getMethod(
            @RequestParam (value="id", defaultValue="1") int id,
            @RequestParam (value="value", defaultValue="Hello World") String value) {
        return new HelloEntity(id, value);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/hello")
    public HelloPostResp postMethod(@RequestBody HelloEntity param) {
        return new HelloPostResp(1, param);
    }

}
