package hungson.net.restful;

public class HelloPostResp {

    private int responseCode;
    private HelloEntity data;

    public HelloPostResp(int responseCode, HelloEntity data) {
        this.responseCode = responseCode;
        this.data = data;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public HelloEntity getData() {
        return data;
    }
}
