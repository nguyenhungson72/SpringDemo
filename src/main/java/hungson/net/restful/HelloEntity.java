package hungson.net.restful;

import java.io.Serializable;

public class HelloEntity implements Serializable {

    private int id;
    private String value;

    public HelloEntity() {}

    public HelloEntity(int id, String value) {
        this.id = id;
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
