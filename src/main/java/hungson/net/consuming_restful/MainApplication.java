package hungson.net.consuming_restful;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.jws.soap.SOAPBinding;
import javax.net.ssl.SSLContext;
import java.security.cert.X509Certificate;

public class MainApplication {

    private static final Logger log = LoggerFactory.getLogger(MainApplication.class);

    public static void main(String[] args) {
        try {
//            new MainApplication().demoGet();
            new MainApplication().demoPost();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public RestTemplate restTemplate() throws Exception {
        TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

        SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .build();

        SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

        CloseableHttpClient httpClient = HttpClients.custom()
                .setSSLSocketFactory(csf)
                .build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

        requestFactory.setHttpClient(httpClient);

        RestTemplate restTemplate = new RestTemplate(requestFactory);

        return restTemplate;
    }

    private void demoGet() throws Exception {
        String urlGet = "https://reqres.in/api/users";

//        Sử dụng cho URL HTTP
//        RestTemplate restTemplate = new RestTemplate();

        //Sử dụng cho URL HTTPs
        RestTemplate restTemplate = this.restTemplate();

        UserGetResp response = restTemplate.getForObject(urlGet, UserGetResp.class);

        log.info("Page >> " + response.getPage());
        log.info("Per Page >> " + response.getPer_page());
        log.info("Total >> " + response.getTotal());
        log.info("Total Page >> " + response.getTotal_pages());

        response.getData().forEach(item -> {
            log.info(String.format("id: %s, Full Name: %s %s, avatar: %s",
                    item.getId(),
                    item.getFirst_name(),
                    item.getLast_name(),
                    item.getAvatar()));
        });
    }

    private void demoPost() throws Exception {
        String urlPost = "https://reqres.in/api/users";
//        Sử dụng cho URL HTTP
//        RestTemplate restTemplate = new RestTemplate();

        //Sử dụng cho URL HTTPs
        RestTemplate restTemplate = this.restTemplate();

        //Set data for request
        UserPostReq params = new UserPostReq();
        params.setName("Hung Son");
        params.setJob("Software Engineer");
        HttpEntity<UserPostReq> bodyEntity = new HttpEntity<>(params);

        UserPostResp response = restTemplate.postForObject(urlPost, bodyEntity, UserPostResp.class);
        log.info("Name >> " + response.getName());
        log.info("Job >> " + response.getJob());
        log.info("ID >> " + response.getId());
        log.info("Created at >> " + response.getCreatedAt());
    }

}
